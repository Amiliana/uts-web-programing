@extends('layouts.main')

@section('container')
  <h1 class="text-center mb-5">Data Mahasiswa UIM</h1>
    <div class="container">
    <a href="/tambahmahasiswa" type="button" class="btn btn-primary">Tambah Data</a>
      <div class="row g-3 align-items-center mt-4">
        <div class="col-auto">
          <!-- <form action="/mahasiswa" method="GET">
            <input type="search" id="inputPassword6" name="search" class="form-control" aria-describedby="passwordHelpInline">
          </form> -->
          <form class="d-flex">
              <input class="form-control me-2" type="search" aria-label="Search" id="inputPassword6" name="search" class="form-control" aria-describedby="passwordHelpInline">
              <button class="btn btn-primary" type="submit">Search</button>
          </form>
        </div>
      </div>
        

      <div class="row">
        <table class="table" style="margin : 20px">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama Mahasiswa</th>
              <th scope="col">Jenis Kelamin</th>
              <th scope="col">NPM</th>
              <th scope="col">Diinput</th>
              <th scope="col">Keterangan</th>

            </tr>
          </thead>
          <tbody>
          @php
            $no =1;
          @endphp
          @foreach($data as $row)  
            <tr>
              <th scope="row">{{$no++}}</th>
              <td>{{$row->nama}}</td>
              <td>{{$row->jeniskelamin}}</td>
              <td>{{$row->nim}}</td>
              <td>{{$row->created_at }}</td>
              <td>
                <a href="/munculkandata/{{$row->id}}"  class="btn btn-primary">Ubah</a>
                <a href="/hapusdata/{{$row->id}}" class="btn btn-warning" onClick='return confirm("Apakah anda ingin menghapus data {{$row->nama}} ?")'>Hapus</a>                 
              </td>
            </tr>
          @endforeach            
              
          </tbody>
        </table> 
      </div>
    </div>
@endsection