@extends('layouts.main')

@section('container')
  <h1 class="text-center mb-5">Tambah Data Mahasiswa</h1>
    <div class="container">
    
      <div class="row justify-content-center">
          <div class="col-9">
            <div class="card">
              <div class="card-body">
              <form action = "/insertdata" method="POST" enctype="multipart/form-data">
                    @csrf
                  <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                      <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">         
                  </div>
                  <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Jenis Kelamin</label>
                      <select class="form-select" name="jeniskelamin" aria-label="Default select example">
                          <option selected>Pilih Jenis Kelamin</option>
                          <option value="laki-laki">Laki-laki</option>
                          <option value="perempuan">Perempuan</option>
                      </select>
                  </div>
                  <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">NPM Mahasiswa</label>
                      <input type="number" name="nim" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">         
                  </div>
                  
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
        
      </div>
    </div>
@endsection