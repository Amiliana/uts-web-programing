<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    Public function index(Request $request){
        if($request->has('search')){
            $data = Mahasiswa::where('nama','LIKE','%'. $request->search.'%')->paginate();        
        }else{
            $data = Mahasiswa::paginate();
        }
            
       // dd($data);
        return view('datamahasiswa',compact('data'));

    }
    public function tambahmahasiswa(){
        return view('tambahdata');
    }
    public function insertdata(Request $request){
        //dd($request->all());
        Mahasiswa::create($request->all());
        return redirect()->route('mahasiswa');
    }
    public function munculkandata($id){
        $data = Mahasiswa::find($id);
        //dd($data);
        return view('munculdata', compact('data'));
    }
    public function updatedata(Request $request, $id){
        $data=Mahasiswa::find($id);
        $data->update($request->all());
        return redirect()->route('mahasiswa');
    }
    public function hapusdata($id){
        $data=Mahasiswa::find($id);
        $data->delete();
        return redirect()->route('mahasiswa');
    }
}
